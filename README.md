## Project Details

**Live Demo**

- https://larryg.herokuapp.com/aerobotics/find-missing-trees-for-orchard/216269

**Languages Stack**

- Built with **Python3.6** and **Django1.11**
- Deployed using **Docker** and **Bitbucket Pipeline**
- Can be run locally using **Django** internal server with **python manage.py runserver** on url **127.0.0.1:8000**
- Can be run locally using **Docker Compose** and the command **docker-compose up**

**To execute**

1. Clone the repo
2. Create a Python Virtual Environment for the project's components with **python -m venv .env**
3. Activate Virtual Environment with **source .env/bin/activate**
4. Install project's components with **pip install -r requirements.txt**
5. Test project's setup with **python manage.py check**
6. Start local server to access project using **python manage.py runserver**
7. Test API Endpoint on any internet browser via the URL **http://127.0.0.1:8000/find-missing-trees-for-orchard​/216269**
8. Test API Endpoint on your terminal via the Curl command **curl -X GET "http://127.0.0.1:8000/find-missing-trees-for-orchard​/216269/" -H "accept: application/json"**

**Website Docker Commands**

- ```docker build -t website_api .```
- ```docker tag website_api <docker_username>/website_api:production```
- ```docker push <docker_username>/website_api:production```
- ```docker pull <docker_username>/website_api:production```
- ```docker run --name production-website --restart=unless-stopped -p 8000:8000 -v website-storage:/website_api -t -d <docker_username>/website_api:production```

**SSL Certificates**

- Project run with **Gunicorn** and **Nginx**
- Configuration files located in folder **webserver**
- Webserver deployment file located in **webserver/Dockerfile**

**Webserver Docker Commands**

- ```docker build -t website_nginx .```
- ```docker tag website_nginx <docker_username>/website_api:nginx```
- ```docker push <docker_username>/website_api:nginx```
- ```docker pull <docker_username>/website_api:nginx```
- ```docker run --name production-webserver --restart=unless-stopped -p 80:80 -p 443:443 -p 8080:8080 -v /var/lib/docker/volumes/website-storage/_data:/usr/share/nginx/html:ro -d <docker_username>/website_api:nginx```

**Installing Certificates**

- ```docker exec production-webserver ./certbot-auto --nginx -n -m ssl@test.com -d <website_domain> --agree-tos```

from django.conf.urls import url 
from . import views

app_name = 'website_api'

urlpatterns=[
	url(r'^$', views.index, name='index'),
	url(r'^find-missing-trees-for-orchard​/(?P<orchard_id>\w+)/$', views.receive_data, name='receive_data')
]
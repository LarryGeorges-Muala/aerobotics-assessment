import os
import csv
import json
import math
import operator
import requests
from math import sin, cos, sqrt, atan2, radians
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponseRedirect, HttpRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt
try:
	from django.urls import reverse
except:
	from django.core.urlresolvers import reverse
from django.utils import timezone
from website_api import models


def calculate_distance_between_coordinates(lat1, lon1, lat2, lon2):
    ''' Distance In Meters Between 2 Coordinates '''
    R = 6373.0
    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = R * c * 1000
    return distance


def rotate_coordinates(origin, point, angle):
    ''' Coordinates Rotation By Given Angle '''
    ox, oy = origin
    px, py = point
    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy


def sort_by_latitude(val):
    ''' Return Latitude Value '''
    return val[0] 


def call_aerobotics_api(api_token, request_url):
    ''' Call Aerobotics And Return Json Response '''
    call_result = None
    response_data = {
        'code': 400,
        'message': '',
        'current_trees_count': 0,
        'new_trees_count': 0,
        'missing_trees_count': 0,
        'missing_trees': []
    }
    try:
        headers_components = {
            'Content-Type': 'application/json',
            'Authorization': api_token
        }
        response = requests.get(
            request_url,
            headers=headers_components
        )
        ''' Extract Response '''
        call_result = response.json()
        ''' Assign Handlers '''
        response_data['code'] = response.status_code
        response_data['message'] = str(call_result)
    except Exception as error:
        print(error)

    return call_result, response_data


def receive_data(request, orchard_id):
    ''' Init Data '''
    orchard_trees_count = 0
    orchard_previous_trees_count = 0
    data = {
        'code': 400,
        'message': '',
        'orchard_id': orchard_id,
        'current_trees_count': 0,
        'new_trees_count': 0,
        'missing_trees_count': 0,
        'missing_trees': []
    }

    ''' Data Processing '''
    try:
        if orchard_id:
            
            ''' API Token '''
            api_token = '1566394169B0EJX2MGAVKVUGGKEMKZBMND9A7VCR'

            ''' Get Tree Surveys of Orchard '''
            request_url = 'https://sherlock.aerobotics.com/developers/treesurveys/?survey__orchard_id={}'.format(
                str(orchard_id)
            )

            ''' Get Aerobotics Data '''
            orchard_survey_data, response_data = call_aerobotics_api(
                api_token,
                request_url
            )

            ''' Endpoint Error Handler '''
            if not orchard_survey_data or response_data['code'] != 200:
                JsonResponse(response_data)

            if orchard_survey_data:
                ''' Current Survey Tree Data '''
                data = orchard_survey_data.get('results')

                ''' Extract Trees Coordinates '''
                tuple_list = []
                missing_trees = []
                for entry in data:
                    temp_tuple = (entry['latitude'], entry['longitude'])
                    tuple_list.append(temp_tuple)
                tuple_list.sort(key=sort_by_latitude, reverse=True)

                ''' Rotate Orchard '''
                new_temp_list = tuple_list
                tuple_list = []
                rotation_angle = -11.6
                tuple_list.append(new_temp_list[0])
                for i in range(1, len(new_temp_list)):
                    a, b = rotate_coordinates(new_temp_list[0], new_temp_list[i], math.radians(rotation_angle))
                    temp_tuple = (a, b)
                    tuple_list.append(temp_tuple)
                tuple_list.sort(key=sort_by_latitude, reverse=True)

                ''' Group Trees By Proximity '''
                temp_list = []
                close_trees_dict = {}
                shortest_distance = 7
                shortest_distance = (shortest_distance + (shortest_distance * 2)) / 2

                for sample_tree in tuple_list:

                    right_adjacent_trees = []
                    left_adjacent_trees = []
                    up_adjacent_trees = []
                    down_adjacent_trees = []

                    for coord in tuple_list:
                        if sample_tree[0] != coord[0] and sample_tree[1] != coord[1]:
                            ''' Calculate Distance '''
                            distance_from_sample = calculate_distance_between_coordinates(
                                sample_tree[0],
                                sample_tree[1],
                                coord[0],
                                coord[1]
                            )

                            ''' Check Proximity '''
                            if distance_from_sample < shortest_distance or distance_from_sample < (shortest_distance * 2) or distance_from_sample < (shortest_distance * 3):
                                temp_dict = {
                                    'sample_tree': sample_tree,
                                    'close_tree': coord,
                                    'close_distance': distance_from_sample
                                }

                                ''' Up Side '''
                                if temp_dict['sample_tree'][0] < temp_dict['close_tree'][0]:
                                    up_margin = (temp_dict['close_tree'][1] - temp_dict['sample_tree'][1]) * 1000
                                    if up_margin < 0.07 and up_margin > -0.05:
                                        if temp_dict not in up_adjacent_trees:
                                            up_adjacent_trees.append(temp_dict)

                                ''' Down Side '''
                                if temp_dict['sample_tree'][0] > temp_dict['close_tree'][0]:
                                    down_margin = (temp_dict['close_tree'][1] - temp_dict['sample_tree'][1]) * 1000
                                    if down_margin < 0.05 and down_margin > -0.05:
                                        if temp_dict not in down_adjacent_trees:
                                            down_adjacent_trees.append(temp_dict)
                                
                                ''' Right Side '''
                                if temp_dict['sample_tree'][1] < temp_dict['close_tree'][1]:
                                    right_margin = (temp_dict['close_tree'][0] - temp_dict['sample_tree'][0]) * 1000
                                    if right_margin < 0.05 and right_margin > -0.05:
                                        if temp_dict not in right_adjacent_trees:
                                            right_adjacent_trees.append(temp_dict)

                                ''' Left Side '''
                                if temp_dict['sample_tree'][1] > temp_dict['close_tree'][1]:
                                    left_margin = (temp_dict['close_tree'][0] - temp_dict['sample_tree'][0]) * 1000
                                    if left_margin < 0.05 and left_margin > -0.05:
                                        if temp_dict not in left_adjacent_trees:
                                            left_adjacent_trees.append(temp_dict)

                    close_trees_dict[str(sample_tree)] = {
                        'up': up_adjacent_trees,
                        'down': down_adjacent_trees,
                        'left': left_adjacent_trees,
                        'right': right_adjacent_trees
                    }

                ''' Check For Missing Trees '''
                for keys, values in close_trees_dict.items():

                    temp_distance_holder = []
                    for entry in values['up']:
                        temp_distance_holder.append(entry['close_distance'])
                    
                    if temp_distance_holder and len(temp_distance_holder) > 1:
                        min_distance = min(temp_distance_holder)
                        if min_distance > shortest_distance:
                            for entry in values['up']:
                                if entry['close_distance'] == min_distance:
                                    temp_tuple = ((entry['sample_tree'][0] + entry['close_tree'][0]) / 2, (entry['sample_tree'][1] + entry['close_tree'][1]) / 2)
                                    if temp_tuple not in missing_trees:
                                        missing_trees.append(temp_tuple)  

                    temp_distance_holder = []
                    for entry in values['down']:
                        temp_distance_holder.append(entry['close_distance'])
                    
                    if temp_distance_holder and len(temp_distance_holder) > 1:
                        min_distance = min(temp_distance_holder)
                        if min_distance > shortest_distance:
                            for entry in values['down']:
                                if entry['close_distance'] == min_distance:
                                    temp_tuple = ((entry['sample_tree'][0] + entry['close_tree'][0]) / 2, (entry['sample_tree'][1] + entry['close_tree'][1]) / 2)
                                    if temp_tuple not in missing_trees:
                                        missing_trees.append(temp_tuple)

                    temp_distance_holder = []
                    for entry in values['left']:
                        temp_distance_holder.append(entry['close_distance'])
                    
                    if temp_distance_holder and len(temp_distance_holder) > 1:
                        min_distance = min(temp_distance_holder)
                        if min_distance > shortest_distance:
                            for entry in values['left']:
                                if entry['close_distance'] == min_distance:
                                    temp_tuple = ((entry['sample_tree'][0] + entry['close_tree'][0]) / 2, (entry['sample_tree'][1] + entry['close_tree'][1]) / 2)
                                    if temp_tuple not in missing_trees:
                                        missing_trees.append(temp_tuple)

                    temp_distance_holder = []
                    for entry in values['right']:
                        temp_distance_holder.append(entry['close_distance'])
                    
                    if temp_distance_holder and len(temp_distance_holder) > 1:
                        min_distance = min(temp_distance_holder)
                        if min_distance > shortest_distance:
                            for entry in values['right']:
                                if entry['close_distance'] == min_distance:
                                    temp_tuple = ((entry['sample_tree'][0] + entry['close_tree'][0]) / 2, (entry['sample_tree'][1] + entry['close_tree'][1]) / 2)
                                    if temp_tuple not in missing_trees:
                                        missing_trees.append(temp_tuple)  

                ''' Rotate To Original Axis '''
                if missing_trees:
                    new_temp_list = missing_trees
                    missing_trees = []
                    rotation_angle = -(rotation_angle)
                    for missing_entry in new_temp_list:
                        a, b = rotate_coordinates(tuple_list[0], missing_entry, math.radians(rotation_angle))
                        temp_tuple = (a, b)
                        missing_trees.append(temp_tuple)

                ''' Remove Duplicate Entries '''
                if missing_trees:
                    missing_trees.sort(key=sort_by_latitude, reverse=True)
                    clean_list = []
                    for i in range(len(missing_trees)):
                        try:
                            margin = (missing_trees[i][0] - missing_trees[i + 1][0]) * 1000
                            if margin > 0.002:
                                temp_dict = {
                                    'lat': missing_trees[i][0],
                                    'lng': missing_trees[i][1]
                                }
                                clean_list.append(temp_dict)
                        except Exception as error:
                            print(error)
                            temp_dict = {
                                'lat': missing_trees[i][0],
                                'lng': missing_trees[i][1]
                            }
                            clean_list.append(temp_dict)
                    
                    missing_trees = clean_list

                ''' Assign Results '''
                data = {
                    'code': response_data['code'],
                    'message': 'Orchard missing trees report successful',
                    'total_trees_count': orchard_survey_data.get('count', 0),
                    'missing_trees_count': len(missing_trees),
                    'missing_trees': missing_trees
                }

    except Exception as error:
        print(error)
        data['message'] = str(error)
    
    return JsonResponse(data)


def index(request):
	return render(request, 'website_api/index.html', {})

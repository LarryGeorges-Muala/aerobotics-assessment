FROM ubuntu:18.04

WORKDIR /website_api

COPY requirements.txt ./

RUN apt-get update -y

RUN apt-get upgrade -y

RUN apt-get install -y python3.6

RUN apt-get install -y python3-pip

RUN apt-get install -y python3-dev libpython3-dev

RUN apt-get install -y default-libmysqlclient-dev

RUN apt-get install -y python3-mysqldb

RUN apt-get install build-essential -y

RUN apt-get install -y mysql-client

RUN apt-get install -y wget

RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

WORKDIR /etc/apt/sources.list.d

RUN touch pgdg.list

RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" > pgdg.list

RUN cat /etc/apt/sources.list.d/pgdg.list

WORKDIR /website_api

RUN apt-get update -y

RUN apt-get -y install postgresql-client-11

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata

RUN echo "Africa/Johannesburg" > /etc/timezone

RUN dpkg-reconfigure -f noninteractive tzdata

RUN apt-get install build-essential -y

RUN apt-get install -y libcurl4-openssl-dev

RUN apt-get install -y openssh-server

RUN apt-get install -y curl vim

RUN apt-get install -y iputils-ping

RUN apt-get install -y python3.6-minimal \
        && apt-get install python-pip -y

RUN pip3 install -r requirements.txt

COPY . .

COPY /webserver/gunicorn.socket /etc/systemd/system/gunicorn.socket
RUN chmod +x /etc/systemd/system/gunicorn.socket

COPY /webserver/gunicorn.service /etc/systemd/system/gunicorn.service
RUN chmod +x /etc/systemd/system/gunicorn.service

ENV PYTHONUNBUFFERED=TRUE

RUN python3 manage.py check

RUN python3 manage.py migrate

RUN python3 manage.py collectstatic --no-input

ENV PYTHONPATH "${PYTHONPATH}:/website_api"

# Clean Unnecessary Files
RUN apt-get clean autoclean
RUN apt-get autoremove -y
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN rm -rf /var/lib/{apt,dpkg,cache,log}/

# Expose Ports
EXPOSE 5432 3306 8000 8080 80 443

# Run App on Server
# CMD ["gunicorn", "--bind", "0.0.0.0:8000", "--env", "DJANGO_SETTINGS_MODULE=assessment.settings", "assessment.wsgi", "--daemon", "--reload", "--log-level", "debug"]

# Run App Locally
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]